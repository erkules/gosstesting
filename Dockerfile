FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
ARG CI_COMMIT_SHA
ARG COMMIT_USER
LABEL COMMIT_USER=${COMMIT_USER}
LABEL CI_COMMIT_SHA=${CI_COMMIT_SHA}
RUN apt-get update
RUN apt-get install -y nginx-light
RUN useradd -G tty www 
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./default    /etc/nginx/sites-enabled/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
RUN chown www: -R /var/cache
RUN chown www:  /var/lib/nginx
USER www
ENTRYPOINT ["/usr/sbin/nginx","-g","daemon off;"]
